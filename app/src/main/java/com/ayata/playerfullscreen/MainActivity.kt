package com.ayata.playerfullscreen

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import com.ayata.playerfullscreen.databinding.ActivityMainBinding
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.MimeTypes
import com.google.android.exoplayer2.util.Util


const val HLS_STATIC_URL1 = "https://nameavyaas.obs.ap-southeast-3.myhuaweicloud.com/uploads/Alphabet%20Series-MAT-Shankar%20Kandel.m4v?AWSAccessKeyId=QZXQGIRDHRD0WN672Y8X&Expires=1658072034&x-amz-security-token=gQ5hcC1zb3V0aGVhc3QtMYeyHfvrisB5NxWZDowkM8hlZ8ZEVC-D9J8ve1RzIiD9he9yLE2iKOjnfUeRfycYyjyDcmaJM7z9FZDjCX9-PvRCAERVbElyyDwfdN2-X_z55Br4dnOk6auKuWDlmJclX3pcuDxkPgnXUNJHyDaYkR78wc6jy4HtoeYDHfjV_4cR7ZBdhi_lHrpfzn-3lyKBShHAuMxvYfugD3P1psx7SEp9aqYHg6Mz0C7Sah5j9qEkTQ4rJPwgOcHbl3DeTQ6J_ZGZ9QQMs8B030zzs8jiPTCm4pgEYk62XimTqasagAqCwxtrpoWXOyLILSY3nhWp2XM_SiwwhcynDW4kLHieiCfqScPSRuP5BV_da6beMgSNoFugxANLjWf5csGTlEQxZ1wpr6tiBPxSQSuhZCkYIDeG0eTEm4avyFPKS0y5FA1WCpagCCHgzjZP3s3FPL43V4wzoXmtok0k2UVSjv2pANZjcLYiTTArYbPZ0P3klSOeI_cvEUyTJMiD6FcZrNHm292DTKNqmnURA7EH6hnZkShPqCr-pLtVIbp3lLUy_fdDJzvgYyIPMlln6_0qaZEkHx5L0WxQ2TRfDQwWaBnTpkAmxK8D_CKiDFdN0FPvrz0ZTfZ9YaoO5cjMNyr7f7CNNqLVoKN_mbfyOHQIY0VjfWb8ncqrWHb1-i7AvFNx6SpzUIvIgIfDkUKcOx6bC0LNB9-Qtr3KYslpb23wpG_eR7UesJw_EK4PtUvERIpL0bvnS6BhtrU34VU3-aLi2cKvZckHH95iQ3-2b2673KaiApRA-MUazk1P7cU-tLSGc0U1g8OcZUlQjuBRqJAo6bVJXXC46qWJCx4lmdHWL5xTfELmLQexRZhslmKN4U7nJTzmzDeP38zJOcqIfSYnlxsH5Xw%3D&Signature=tcLtn8/CMaKIePh7%2B%2B7Obdmvp4I%3D"
const val STATE_RESUME_WINDOW = "resumeWindow"
const val STATE_RESUME_POSITION = "resumePosition"
const val STATE_PLAYER_FULLSCREEN = "playerFullscreen"
const val STATE_PLAYER_PLAYING = "playerOnPlay"

class MainActivity : AppCompatActivity() {
    val TAG = "MainActivity"

    private lateinit var exoPlayer: SimpleExoPlayer
    private lateinit var dataSourceFactory: DataSource.Factory
    private lateinit var playerView: PlayerView
    private lateinit var exoFullScreenIcon: ImageView
    private lateinit var exoFullScreenBtn: FrameLayout
    private lateinit var imageViewsetting: ImageView
    private lateinit var textplayback:TextView
    private lateinit var btnback:ImageView

    private var currentWindow = 0
    private var playbackPosition: Long = 0
    private var isFullscreen = false
    private var isPlayerPlaying = true
    private var speed = 1.0f
    lateinit var mainBinding: ActivityMainBinding
    private val mediaItem = MediaItem.Builder()
        .setUri(HLS_STATIC_URL1)
        .setMimeType(MimeTypes.APPLICATION_MP4VTT)
        .build()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mainBinding.root)
        playerView = mainBinding.playerView
        exoFullScreenBtn = playerView.findViewById(R.id.exo_fullscreen_button)
        exoFullScreenIcon = playerView.findViewById(R.id.exo_fullscreen_icon)
        imageViewsetting = playerView.findViewById(R.id.exo_settingss)
        textplayback=playerView.findViewById(R.id.textplayback);
        btnback=playerView.findViewById(R.id.btnBack)

        dataSourceFactory =
            DefaultDataSourceFactory(this, Util.getUserAgent(this, "playerfullscreen"))
        initFullScreenButton()

        if (savedInstanceState != null) {
            currentWindow = savedInstanceState.getInt(STATE_RESUME_WINDOW)
            playbackPosition = savedInstanceState.getLong(STATE_RESUME_POSITION)
            isFullscreen = savedInstanceState.getBoolean(STATE_PLAYER_FULLSCREEN)
            isPlayerPlaying = savedInstanceState.getBoolean(STATE_PLAYER_PLAYING)
        }


    }

    private fun showPlayerSpeedDialog(imageView: ImageView) {
        val playerSpeedArrayLabels = arrayOf("0.5x", "1.2x", "1.0x", "2.0x", "2.5x", "4.0x")
        //anchoring the imageview
        val popupMenu = PopupMenu(this@MainActivity, imageView)
        for (i in playerSpeedArrayLabels.indices) {
            popupMenu.menu.add(i, i, i, playerSpeedArrayLabels[i])
        }
        popupMenu.setOnMenuItemClickListener { item ->
            val itemTitle: CharSequence = item.title
            val playbackSpeed = itemTitle.subSequence(0, 3).toString().toFloat()
            changePlayerSpeed(playbackSpeed, itemTitle.subSequence(0, 3).toString())
            false
        }
        popupMenu.show()
    }

    private fun changePlayerSpeed(speed: Float, speedLabel: String) {
        this.speed = speed
        val param = PlaybackParameters(speed, 1.0f)
        textplayback.setText(speed.toString()+"x");
        exoPlayer.playbackParameters = param

    }


    private fun initPlayer() {
        val trackSelector=DefaultTrackSelector(this).apply {
            setParameters(buildUponParameters().setMaxVideoSizeSd())
        }

        //you can add media sourc ftom here and set media type
//         val mediaItem = MediaItem.Builder()
//            .setUri(HLS_STATIC_URL1)
//            .setMimeType(MimeTypes.APPLICATION_MP4)
//            .build()

        exoPlayer=SimpleExoPlayer.Builder(this)
            .setTrackSelector(trackSelector)
            .build()

        exoPlayer.apply {
            playWhenReady=isPlayerPlaying
            seekTo(currentWindow,playbackPosition)
            setMediaItem(mediaItem,false)
            prepare()
        }
        playerView.player = exoPlayer
        if (isFullscreen) openFullscreen()
    }


    private fun releasePlayer()
    {
        isPlayerPlaying = exoPlayer.playWhenReady
        playbackPosition = exoPlayer.currentPosition
        currentWindow = exoPlayer.currentWindowIndex
        exoPlayer.release()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(STATE_RESUME_WINDOW, exoPlayer.currentWindowIndex)
        outState.putLong(STATE_RESUME_POSITION, exoPlayer.currentPosition)
        outState.putBoolean(STATE_PLAYER_FULLSCREEN, isFullscreen)
        outState.putBoolean(STATE_PLAYER_PLAYING, isPlayerPlaying)
        super.onSaveInstanceState(outState)
    }


    private fun initFullScreenButton() {
        exoFullScreenBtn.setOnClickListener {
            if (!isFullscreen) {
                openFullscreen()
            } else {
                closeFullscreen()
            }
        }

        imageViewsetting.setOnClickListener {
            showPlayerSpeedDialog(imageViewsetting)
        }

        btnback.setOnClickListener {
            onBackPressed()
        }


    }

    @SuppressLint("SourceLockedOrientationActivity")
    private fun openFullscreen() {
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        exoFullScreenIcon.setImageDrawable(
            ContextCompat.getDrawable(
                this,
                R.drawable.ic_fullscreen_shrink
            )
        )
        playerView.setBackgroundColor(ContextCompat.getColor(this, R.color.black))
        val params: LinearLayout.LayoutParams = playerView.layoutParams as LinearLayout.LayoutParams
        params.width = LinearLayout.LayoutParams.MATCH_PARENT
        params.height = LinearLayout.LayoutParams.MATCH_PARENT
        playerView.layoutParams = params
        supportActionBar?.hide()
        hideSystemUi()
        isFullscreen = true
    }

    private fun closeFullscreen() {
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR
        exoFullScreenIcon.setImageDrawable(
            ContextCompat.getDrawable(
                this,
                R.drawable.ic_fullscreen_expand
            )
        )
        playerView.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        val params: LinearLayout.LayoutParams = playerView.layoutParams as LinearLayout.LayoutParams
        params.width = LinearLayout.LayoutParams.MATCH_PARENT
        params.height = 0
        playerView.layoutParams = params
        supportActionBar?.show()
        playerView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        isFullscreen = false
    }


    private fun hideSystemUi() {
        WindowCompat.setDecorFitsSystemWindows(window, false)
        WindowInsetsControllerCompat(window, playerView).let { controller ->
            controller.hide(WindowInsetsCompat.Type.systemBars())
            controller.systemBarsBehavior =
                WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        }
    }


    override fun onStart() {
        super.onStart()
        if (Util.SDK_INT > 23) {
            initPlayer()
            playerView.onResume()
        }
    }

    override fun onResume() {
        super.onResume()
        if (Util.SDK_INT <= 23) {
            initPlayer()
            playerView.onResume()
        }
    }

    override fun onPause() {
        super.onPause()
        if (Util.SDK_INT <= 23) {
            playerView.onPause()
            releasePlayer()
        }
    }

    override fun onStop() {
        super.onStop()
        if (Util.SDK_INT > 23) {
            playerView.onPause()
            releasePlayer()
        }
    }

    override fun onBackPressed() {
        if (isFullscreen) {
            closeFullscreen()
            return
        }
        super.onBackPressed()
    }

}